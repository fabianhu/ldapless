#!/usr/bin/python3

import argparse
import configparser
import io
import logging
import os
import subprocess
from getpass import getpass
from logging import NullHandler
from string import Template
from time import sleep

import paramiko

logging.getLogger('paramiko.transport').addHandler(NullHandler())

# fixme features to be added.
# - add subnet search function for connected clients (just try to connect with ssh)
# - remove client (including uninstall of ldapless user)

hosts_file = "ldapless.hosts"
config_file = "ldapless.ini"

parser = argparse.ArgumentParser(description='LDAPLESS user management tool')
parsergroup = parser.add_mutually_exclusive_group()
parsergroup.add_argument('--add_client', action='store', help=
                         'install ldapless user and ssh certificate to remote '
                         'machine and add machine to database.\n'
                         'required argument: [hostname|IP]')
# action must be "store", if further arguments are needed!
parsergroup.add_argument('--read', action='store_true',
                         help='Run as root: '
                              'Read the user and group database from this machine and put into config file.')
parsergroup.add_argument('--push', action='store_true',
                         help='push the users + passwords to the machines from the config file')

print(os.getcwd())


# noinspection PyPep8Naming
class myMissingHostKeyPolicy(paramiko.AutoAddPolicy):
    def missing_host_key(self, client, hostname, key):
        # return or except in this def.
        print("*** Host:", hostname, "Missing key:", key.get_fingerprint().hex())
        super().missing_host_key(client, hostname, key)
        return


def startup_checks():
    check_and_make_key('Config', 'start_user_id', '1000')
    check_and_make_key('Config', 'end_user_id', '1999')
    check_and_make_key('Config', 'start_group_id', '2000')
    check_and_make_key('Config', 'end_group_id', '2999')
    check_and_make_key('Config', 'ssh_key', '')
    if (config['Config']['ssh_key']) == "":
        print('Error - No Private key stored! Creating one first.')
        create_key()

    check_and_make_section('Clients')
    check_and_make_section('Users')
    check_and_make_section('Secrets')
    check_and_make_section('Groups')
    check_and_make_section('GroupMembers')
    # check, if there is a user, which should not be managed by ID limit, which may be limited after reading users
    for user in config['Users']:
        userid = config.get("Users", user)
        minuser = config['Config']['start_user_id']
        maxuser = config['Config']['end_user_id']
        if userid < minuser or userid > maxuser:
            config.remove_option('Users', user)
            if config.has_option('Secrets', user):
                config.remove_option('Secrets', user)
    # remove orphaned Secrets
    for user in config['Secrets']:
        if not config.has_option('Users', user):
            config.remove_option('Secrets', user)

    for group in config['Groups']:
        groupid = config.get("Groups", group)
        mingroup = config['Config']['start_group_id']
        maxgroup = config['Config']['end_group_id']
        if groupid < mingroup or groupid > maxgroup:
            config.remove_option('Groups', group)
            if config.has_option('GroupMembers', group):
                config.remove_option('GroupMembers', group)
    # remove orphaned GroupMembers
    for group in config['GroupMembers']:
        if not config.has_option('Groups', group):
            config.remove_option('GroupMembers', group)

    if not os.path.isfile(config_file):
        os.mknod(config_file)

    # check, if there is a discrepancy between the known hosts and the ini file.
    # check existence and create if not there
    if not os.path.isfile(hosts_file):
        os.mknod(hosts_file)

    # read hosts from file and rewrite it for removing ones not configured.
    if (len(config.options('Clients'))) > 0:
        ips, names = zip(*config.items('Clients'))
        with open(hosts_file, "r") as f:
            host_lines = f.readlines()
        with open(hosts_file, "w") as f:
            for line in host_lines:
                if line.split(" ")[0] in ips:  # remove hosts not in ini
                    f.write(line)
    else:
        with open(hosts_file, "w") as f:
            f.write('')

    # unknown clients will be added automatically
    # check if file permissions are correct.
    check_permissions(hosts_file, '0o600')
    check_permissions(config_file, '0o600')


# execute remote command
def ssh_exec(addr, cmd) -> (int, str):
    """

    :rtype: (int , str) # exit code and return string

    alternative:
    import subprocess
    subprocess.Popen("ssh {user}@{host} {cmd}".format(user=user, host=host, cmd='ls -l'), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()

    """
    try:
        ssh = paramiko.SSHClient()
        # prepare to check the hosts
        ssh.load_host_keys(hosts_file)  # my local hosts file, only file possible. Putting into ini does not work.
        ssh.set_missing_host_key_policy(myMissingHostKeyPolicy)

        # load rsa key from config
        stringio2 = io.StringIO(config['Config']['ssh_key'])
        private_key = paramiko.RSAKey.from_private_key(stringio2)
        if private_key == '':
            print('Error - No Private key stored! Create one first.')
            exit(1)

        ssh.connect(addr, username='ldapless', pkey=private_key, allow_agent=False, look_for_keys=True)
        sleep(0.1)
        if ssh.get_transport().is_active:
            print("transport is active")

        _, ssh_stdout, ssh_stderr = ssh.exec_command(command=cmd)
        result = ssh_stdout.read().decode('utf-8')
        error = ssh_stderr.read().decode('utf-8')
        retcode = ssh_stdout.channel.recv_exit_status()

        if error:
            print(error)

        # ssh.save_host_keys(hosts_file)  # not necessary, done implicitly
        ssh.close()

        return retcode, result

    except paramiko.BadHostKeyException as e:
        print("Exception: {}".format(e))
        print("Remove key for this host [" + e.hostname + "] from the file " + hosts_file + " manually!")
        exit(1)
        return -1, "Exception: {}".format(e)

    except Exception as e:
        print("ip ", addr, "Exception: {}".format(e))
        return -1, "Exception: {}".format(e)


# execute remote command
def ssh_exec_pwd(addr, user, passw, cmd) -> (int, str):
    """

    :rtype: (int , str) # exit code and return string
    """
    try:
        ssh = paramiko.SSHClient()
        # prepare to check the hosts
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy)  # necessary, otherwise the default policy kicks us out.

        ssh.connect(addr, username=user, password=passw, allow_agent=True, look_for_keys=False)
        stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command=cmd, get_pty=True)
        sleep(0.5)  # wait for password prompt
        stdin.write(passw + '\n')
        stdin.flush()
        result = ssh_stdout.read().decode('utf-8')
        print(result)
        error = ssh_stderr.read().decode('utf-8')
        retcode = ssh_stdout.channel.recv_exit_status()

        if error:
            print(error)

        ssh.close()

        return retcode, result

    except Exception as e:
        print("ip ", addr, "Exception: {}".format(e))
        return -1, "Exception: {}".format(e)


# execute local command as actual user
def local_exec(cmd):
    try:
        prc = subprocess.Popen(cmd, shell=True, executable='/bin/bash', stdout=subprocess.PIPE, text=True)
        prc.wait(1)
        out, err = prc.communicate()
        errcode = prc.returncode
        return errcode, out  # .decode('utf-8')

    except Exception as e:
        print("local Exception: {}".format(e))
        return -1, "Exception: {}".format(e)


# check config section completeness
def check_and_make_section(s):
    if not config.has_section(s):
        config.add_section(s)


# check kex existence and add default if not existing
def check_and_make_key(s, o, v):
    if not config.has_section(s):
        config.add_section(s)
    if not config.has_option(s, o):
        config.set(s, o, v)


def check_permissions(file, perm):
    act_perm = oct(os.stat(file).st_mode & 0o777)
    if act_perm != perm:
        print("File", file, "does not have the required permission of", perm, "Please fix manually.")
        exit(1)


def cmd_add_client(c):
    print("add-client", c)

    user = input("Enter username for remote machine: ")
    passw = getpass("Enter password for remote machine: ")

    # the following is not a comment, even if it looks like one!
    s = Template("""#!/bin/bash
#  auto generated via variable and pushed via ssh. delete directly after read
echo logged in as \\$USER on \\$HOSTNAME with \\$EUID
# rm /tmp/ldapless_install.sh # fixme remove comment
getent passwd $NEWUSER > /dev/null
if [ \\$? -eq 0 ]; then
    echo \\\"the user $NEWUSER exists\\\"
    # exit 1 - nop, continue for updating the keys
else
    echo \\\"Ok, the user does not exist - creating.\\\"
    useradd --system --home /home/$NEWUSER --shell /bin/bash $NEWUSER
    mkdir /home/$NEWUSER
    #can not create system account via: adduser --gecos \\\"\\\" --disabled-password $NEWUSER -
    mkdir /home/$NEWUSER/.ssh
    echo $NEWUSER ALL=NOPASSWD: ALL>>/etc/sudoers
fi
# only one key should have access!
echo \\\"$CERTIFICATE\\\">/home/$NEWUSER/.ssh/authorized_keys
chown -R $NEWUSER:$NEWUSER /home/$NEWUSER
chmod 0700 /home/$NEWUSER/.ssh
chmod 0600 /home/$NEWUSER/.ssh/authorized_keys
echo done with remote stuff on client
""")

    # load rsa key from config
    stringio2 = io.StringIO(config['Config']['ssh_key'])
    private_key = paramiko.RSAKey.from_private_key(stringio2)
    if private_key == '':
        print('Error - No Private key stored! Creating one first.')
        # fixme ask
        create_key()

    var = s.safe_substitute(NEWUSER='ldapless', CERTIFICATE=private_key.get_name() + " " + private_key.get_base64())

    cmd = "echo -e \"" + var + \
          "\">/tmp/ldapless_install.sh ; sudo chmod +x /tmp/ldapless_install.sh ; sudo /tmp/ldapless_install.sh"

    ret, _ = ssh_exec_pwd(c, user, passw, cmd)
    if (ret == 0):
        print("all ok")
        config.set('Clients', c, 'machine_' + c)


# read users from local machine
def cmd_read():
    print('reading users from local machine')

    if not os.geteuid() == 0:
        print("This command requires root permission on this machine.\nYou are not root. Exiting.")
        exit(1)

    config.remove_section('Users')  # delete all users in config
    config.add_section('Users')
    minuser = config['Config']['start_user_id']
    maxuser = config['Config']['end_user_id']
    _, ret = local_exec("getent passwd {" + minuser + ".." + maxuser + "}")
    if ret != 0:
        print("could not read passwd file")
        exit(1)
    for line in ret.splitlines():
        name = line.split(':')[0]
        uid = line.split(':')[2]
        print(uid, name)
        config.set('Users', name, uid)

    config.remove_section('Secrets')  # delete all secrets in config
    config.add_section('Secrets')
    for user in config['Users']:
        print(user)
        _, ret = local_exec("sudo getent shadow " + user)  # returns one newline at the end!
        if ret != 0:
            print("could not read shadow file")
            exit(1)
        ret = ret.rstrip()  # remove newline
        ret = ret.split(':')[1]  # take only the password part
        config.set('Secrets', user, ret)

    config.remove_section('Groups')  # delete all Groups in config
    config.add_section('Groups')
    mingrp = config['Config']['start_group_id']
    maxgrp = config['Config']['end_group_id']
    _, ret = local_exec("getent group {" + mingrp + ".." + maxgrp + "}")
    if ret != 0:
        print("could not read group file")
        exit(1)
    for line in ret.splitlines():
        name = line.split(':')[0]
        uid = line.split(':')[2]
        members = line.split(':')[3]
        print(uid, name)
        config.set('Groups', name, uid)
        config.set('GroupMembers', name, members)


def cmd_push():
    for ip, hostname in config.items('Clients'):  # iterate over machines
        print("pushing to", hostname, ip)

        retcode, result = ssh_exec(ip, "ls")  # check, if connectivity is OK
        if retcode != 0:
            continue

        # iterate over known local users
        for user in config['Users']:
            retcode, result = ssh_exec(ip, "getent passwd " + user)
            if retcode != 0:
                print("User", user, "is not existing on", ip, "generating.")

                # check for occupied ID
                retcode, result = ssh_exec(ip, "getent passwd " + config.get("Users", user))
                if retcode == 0:
                    cli_name = result.split(':')[0]
                    print("\n*** User ID mismatch:\nOn", ip, "user", user, "needs id", config.get("Users", user),
                          "but it is taken by", cli_name, "\nThis must be fixed manually. "
                                                          "\n(see end of the file ldapless.py for a hint)")
                    continue

                # generate user with home
                # fixme postpone to generate the user later, it may be existing with a wrong id.
                # ok, it would crash anyway... so, yolo
                cmd = "sudo useradd -u " + config.get("Users", user) + " -m -U -c \"" + user + "\" " + user
                retcode, result = ssh_exec(ip, cmd)
                if retcode != 0:
                    print(ip, ": Error creating user", user)
                    exit(1)

            # update password
            retcode, result = ssh_exec(ip, "getent passwd " + user)  # repeat to avoid empty result
            cli_name = result.split(':')[0]
            cli_uid = result.split(':')[2]
            # print("user", user, "has local id", config.get("Users", user), "and remote id", cli_uid)
            if cli_uid == config.get("Users", cli_name) and cli_name == user:  # update password
                print(ip, ": refresh password of user", user)
                # build command for replacing the remote shadow
                cmd = 'sudo usermod -p \'' + config['Secrets'][user] + '\' ' + user

                rc, result = ssh_exec(ip, cmd)
            else:
                print("User ID mismatch:\nOn", ip, "user", user, "has local id", config.get("Users", user),
                      "and remote id", cli_uid)
                exit(1)

        # iterate over remote users
        minuser = config['Config']['start_user_id']
        maxuser = config['Config']['end_user_id']
        retcode, ret = ssh_exec(ip, "getent passwd {" + minuser + ".." + maxuser + "}")
        for line in ret.splitlines():
            cli_name = line.split(':')[0]
            cli_uid = line.split(':')[2]
            if not config.has_option('Users', cli_name):  # check user existence
                print(ip, ": user", cli_name, "is unmanaged:", cli_uid)

        # iterate over known local groups
        for group in config['Groups']:
            retcode, result = ssh_exec(ip, "getent group " + group)
            if retcode != 0:
                print("Group", group, "is not existing on", ip, "generating.")

                # check for occupied ID
                retcode, result = ssh_exec(ip, "getent group " + config.get("Groups", group))
                if retcode == 0:
                    cli_name = result.split(':')[0]
                    print("\n*** Group ID mismatch:\nOn", ip, "group", group, "needs id", config.get("Groups", group),
                          "but it is taken by", cli_name, "\nThis must be fixed manually. "
                                                          "\n(see end of the file ldapless.py for a hint)")
                    continue

                # generate group
                cmd = "sudo addgroup --gid " + config.get("Groups", group) + " " + group
                print(cmd)
                retcode, result = ssh_exec(ip, cmd)
                if retcode != 0:
                    print(ip, ": Error creating group", group)
                    exit(1)
            # add group members
            for uig in config['GroupMembers'][group].split(','):
                cmd = "sudo usermod -a -G " + group + " " + uig
                print(cmd)
                retcode, result = ssh_exec(ip, cmd)
                if retcode != 0:
                    print(ip, ": Error adding user "+uig+" to group", group)
                    exit(1)

def create_key():
    print('creating new key')
    # check, if we have key already
    # is key exists, ask user to abort
    if config['Config']['ssh_key'] != '':
        print("To change key, remove key from config file", config_file,
              "manually. \nConnection to all clients will be lost.\n no changes were made.")
        exit(1)
    # create new key
    key = paramiko.RSAKey.generate(2048)

    # store rsa key to config
    stringio = io.StringIO()
    key.write_private_key(stringio)
    config.set('Config', 'ssh_key', stringio.getvalue())


# first action on start: read config file
config = configparser.ConfigParser()
config.read(config_file)

startup_checks()  # after config read

args = parser.parse_args()

if args.add_client:
    cmd_add_client(args.add_client)
elif args.read:
    cmd_read()
elif args.push:
    cmd_push()
else:
    parser.print_usage()

# at the end of all operations, we write the config file
with open(config_file, 'w') as f:
    config.write(f)
print('finished.')

######################################################################################################################
# development stuff below will be removed ############################################################################
######################################################################################################################
######################################################################################################################
######################################################################################################################
exit(0)  # ###########################################################################################################
######################################################################################################################
######################################################################################################################
######################################################################################################################
######################################################################################################################
######################################################################################################################
######################################################################################################################


"""
    How to change a user ID:
    
    The Debian/Ubuntu policy is that if there is a user jim with user ID 1001, 
    there is also a group jim with group ID 1001. This solution also updates those group IDs.

    Enable the root account:

    sudo nano /etc/ssh/sshd_config
    Search for PermitRootLogin and change it to yes.

    sudo passwd root

    If the user is logged in, then log out (also on virtual terminals)
    Go to VT1: Ctrl-Alt-F1

    Log in as root and run this with the user name and old/new UID supplied:

    # put the information we need in variables
    username=...
    old_uid=`id -u $username`  # looks up current (old) uid
    new_uid=...

    # update the user ID and group ID for $username
    usermod -u $new_uid $username
    groupmod -g $new_uid $username

    # update the file ownerships
    # NB: you cannot combine the next two chowns, or files where 
    # only the uid xor the gid matches won't be updated  
    chown -Rhc --from=$old_uid $new_uid /    # change the user IDs
    chown -Rhc --from=:$old_uid :$new_uid /  # change the group IDs

    Log out
    Log in as $username

    Disable the root account:

    sudo passwd -dl root

    sudo nano /etc/ssh/sshd_config
    Search for PermitRootLogin and change it to no.
"""
