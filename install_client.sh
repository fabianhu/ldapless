#!/bin/bash
# This file is already superfluous and included into the ldapless.py file. Add machine name and IP to config file automagically.
echo "This is the installer of the LDAPLESS user management on a client."
echo ""

if [ $# -ne 1 ]; then
	echo "Usage: $(basename -- "$0") USER@CLIENT_MACHINE]"
	echo ""
	echo "To be run on SERVER only."
	echo "The USER has to have root privileges on client machine."
	echo "We use the SSH keys from the actual user ($USER) for future access."
	exit 1
fi

USERatHOST=$1

# the well known user name which will be added to the client machine.
NEWUSER="ldapless"

# temprary script filename on client machine
TEMPURA="/tmp/inst_ldapless.sh"

# check if keyfile workaround exists
[[ ! -f ./ldapless.key ]] && ./ldapless.py --create_key

# read local ssh keys #  version included into ldapless.py uses config file.
ssh-keygen -y -f ./ldapless.key > ./ldapless.pub
CERTIFICATE=$(<./ldapless.pub)

# ssh-copy-id is not possible, as new user does not exist yet.

# add "ldapless" user as system user. He even gets a home for future use.
# all " are double escaped to make them survive the transfer.
VARIABLE="
#!/bin/bash
# auto generated via variable and pushed via ssh. delete directly after read
echo logged in as \$USER on \$HOSTNAME
# rm $TEMPURA
getent passwd $NEWUSER > /dev/null
if [ \\\$? -eq 0 ]; then
    echo \\\"the user $NEWUSER exists\\\"
    # exit 1 - nop, continue for updating the keys
else
    echo \\\"Ok, the user does not exist - creating.\\\"
    useradd --system --home /home/$NEWUSER --shell /bin/bash $NEWUSER
    mkdir /home/$NEWUSER
    #can not create system account via: adduser --gecos \\\"\\\" --disabled-password $NEWUSER -
    mkdir /home/$NEWUSER/.ssh
    echo $NEWUSER ALL=NOPASSWD: ALL>>/etc/sudoers
fi
echo $CERTIFICATE>/home/$NEWUSER/.ssh/authorized_keys
chown -R $NEWUSER:$NEWUSER /home/$NEWUSER
chmod 0700 /home/$NEWUSER/.ssh
chmod 0600 /home/$NEWUSER/.ssh/authorized_keys
echo done with remote stuff on client
"

# now stuff is collected and we do all in one session
echo "You may have to enter the password twice "
echo "- ssh session opening and"
echo "- sudo for user generation during ssh session"

# use specific key
# - no, we go by password !!! ssh -o "IdentitiesOnly=yes" -i ldapless.key -t $USERatHOST "echo -e \"$VARIABLE\">$TEMPURA ; sudo chmod +x $TEMPURA ; sudo $TEMPURA"
ssh -t $USERatHOST "echo -e \"$VARIABLE\">$TEMPURA ; sudo chmod +x $TEMPURA ; sudo $TEMPURA"
