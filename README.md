# ldapless
The ldap-less centralized user management for small networks

Target is, to have a central user authenticationn database, from which the clients are synchronized,
without all the LDAP mess getting the configuration right on all implementations.
Always fun are the telling error messages like "error: implementation specific"
Main goal is easy installation and simple operation for a small scale environment.

# Instructions

## _Danger_

>Use backup before you play around. 
>Normally this should all work just fine, but your mileage may vary.
>I am not the one to blame, if you locked yourself out of your machines. 
>Maybe you can use the 'ldapless' user to regain access. But do not loose the key.


## Installation

1. put ldapless.py on your main machine, where all users exist, 
save into your preferred storage location for your valuable user data, then:

2. for every host:
  `ldapless.py --add_client hostname_or_ip`
This user must have sudo right on the remote machine. 
This will ask you for the username and password to install the 'ldapless' user.

## User management

1. manage your users on the local machine, where ldapless.py resides.
  To import the users and passwords from your local machine, run:
  `sudo ldapless.py --read_users` 
  This acquires all user names and passwords to be synchronised.

2. push users to all clients
    `sudo ldapless.py --push`

## Expert mode
  look for ldapless.ini after first run.
  You can limit the managed UID range in the ldapless.ini file

# Specifications:

## Should run on
  - Synology (Just noticed, that user management is defective on Synology. They could not keep it standard. 
  So I dropped Synology.)
  + Raspberry Pi Raspian
  + AMD64 Ubuntu
  + Odroid HC2 Ubuntu
  + in general it should work on all Debian based systems.

## Security features
  * all passwords are at least transferred as salted hashes as they appear in /etc/shadow
  * all "hot" information is stored in one .ini file
  * file permissions of ini file and known hosts file are checked
  * all traffic between client and server is ssh-ed.
  * no traces left in bash history and ps (to be confirmed)
  * client needs to trust only his server -> client does not trust any server,
    * secured via ssh access from server to client by private/public key authentication
  * server needs to auth his client -> server does not trust any client
    * server remembers client fingerprint and complains, if changed.

## Server
  * is a normal process, except, it needs `sudo` for collecting the user secrets.
  * does not directly influence the machine it is running on. (fixme to discuss)
  * has one .ini file of
    * user passwd, shadow
    * certificate to access client machine
  * remembers client footprints (unfortunately in external file - complain to paramiko)

## Client
  * just sits and waits
 
# relevant files
  - /etc/passwd -> user existence is read. No state is changed.
  - /etc/shadow -> passwords are read from server and written to clients
  - /etc/groups -> not mirrored [^1].
  - /etc/gshadow -> not mirrored [^1].

[^1]: if there are special rights and group memberships necessary on a machine, these are not touched und must be maintained manually.


# Use cases
## install server
   * server has no running process, ldapless.py is only called by admin. (done)
   * generate ssh key pair for client access (as server user) (done)
   * generate config-file / database of machines. (done)
   * list of users and IDs to replicate (done)

## install on client (all in one script: install_client.sh - call from server as admin user)
   * This is done by install_client.sh run on SERVER. (done)
   * create user "ldapless" on client for ssh access with root rights but without password. (done)
   * drop ssh key from server into ldapless's .ssh dir. (done)
   
## update client user database
   * access on client machine via SSH (ldapless), no process or SW on client. (done)
   * new user - add nonexisting users and /home (done)
   * remove user - manually (done - orphaned users are reported)
   * user group assingnment - manually + machine specific
   * alert on mismatch ID / name (done)
   * new password -> always use password from server database (done)

## change password
   only from server and push to machines. (done)

	
